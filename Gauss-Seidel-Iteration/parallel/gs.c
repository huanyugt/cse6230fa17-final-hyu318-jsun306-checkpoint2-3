#include "gs.h"
#include <petscsys.h>

struct _gs
{
  MPI_Comm comm;
};

int GSCreate(MPI_Comm comm, GS *gs_p)
{
  GS gs = NULL;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscCalloc1(1,&gs); CHKERRQ(ierr);

  gs->comm = comm;

  *gs_p = gs;
  PetscFunctionReturn(0);
}

int GSDestroy(GS *gs_p)
{
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  ierr = PetscFree(*gs_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int GSGetFieldArrays(GS gs, size_t N, struct IndexBlock *iBlock, double **fLocal_p, double **uLocal_p)
{
  MPI_Comm comm;
  size_t   local;
  int      size, rank;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  comm = gs->comm;

  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);

  iBlock->xStart = 0;
  iBlock->xEnd   = N - 1;
  iBlock->yStart = 0;
  iBlock->yEnd   = N - 1;
  iBlock->zStart = (rank * (N - 1)) / size;
  iBlock->zEnd   = ((rank + 1) * (N - 1)) / size;

  local = (iBlock->xEnd - iBlock->xStart) * (iBlock->yEnd - iBlock->yStart) * (iBlock->zEnd - iBlock->zStart);

  ierr = PetscMalloc2(local, fLocal_p, local, uLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int GSRestoreFieldArrays(GS gs, size_t N, struct IndexBlock *iBlock, double **fLocal_p, double **uLocal_p)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscFree2(*fLocal_p, *uLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int GSIterate(GS gs, size_t N, int n_iter, struct IndexBlock *iBlock, const double *fLocal, double *uLocal)
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}
