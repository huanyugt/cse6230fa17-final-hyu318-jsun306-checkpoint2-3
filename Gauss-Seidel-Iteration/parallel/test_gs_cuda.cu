const char help[] = "Test driver for the correctness of Gauss-Seidel iteration implementation";

#include <petscmat.h>
#include "gs.h"

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error %s", cudaGetErrorString(_cerr));} while(0)

static PetscErrorCode PetscGaussSeidelCheck(PetscInt n, PetscInt nits, const size_t *order, const double *f, double *u)
{
  PetscInt       nm1 = n - 1;
  PetscInt       N = nm1 * nm1 * nm1;
  PetscReal      h = 1. / n;
  Mat            K;
  IS             perm, invperm;
  const PetscInt *iorder;
  Vec            B, X;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  {
    PetscInt *iperm, i;

    ierr = PetscMalloc1(N, &iperm);CHKERRQ(ierr);
    for (i = 0; i < N; i++) iperm[i] = (PetscInt) order[i];
    ierr = ISCreateGeneral(PETSC_COMM_SELF, N, iperm, PETSC_OWN_POINTER, &perm);CHKERRQ(ierr);
  }
  ierr = ISSetPermutation(perm);CHKERRQ(ierr);
  ierr = ISInvertPermutation(perm,N,&invperm);CHKERRQ(ierr);
  ierr = MatCreate(PETSC_COMM_SELF, &K);CHKERRQ(ierr);
  ierr = MatSetType(K, MATSEQAIJ);CHKERRQ(ierr);
  ierr = MatSetSizes(K, N, N, N, N);CHKERRQ(ierr);
  ierr = MatSetOption(K, MAT_USE_INODES, PETSC_FALSE);CHKERRQ(ierr);
  ierr = MatSeqAIJSetPreallocation(K, 27, NULL);CHKERRQ(ierr);
  {
    PetscInt    i, j, k, l;
    PetscScalar v[27] = {-1./12., -1./6. , -1./12.,
                         -1./6. ,  0.    , -1./6. ,
                         -1./12., -1./6. , -1./12.,
                         -1./6. ,  0.    , -1./6. ,
                          0.    ,  8./3. ,  0.    ,
                         -1./6. ,  0.    , -1./6. ,
                         -1./12., -1./6. , -1./12.,
                         -1./6. ,  0.    , -1./6. ,
                         -1./12., -1./6. , -1./12.};

    for (l = 0; l < 27; l++) v[l] *= h;
    ierr = ISGetIndices(invperm, &iorder);CHKERRQ(ierr);
    for (k = 0, l = 0; k < nm1; k++) {
      for (j = 0; j < nm1; j++) {
        for (i = 0; i < nm1; i++, l++) {
          PetscInt   a, b, c, d;
          PetscInt   self = iorder[l];
          PetscInt   neigh[27];

          for (c = 0, d = 0; c < 3; c++) {
            for (b = 0; b < 3; b++) {
              for (a = 0; a < 3; a++, d++) {
                PetscInt ni, nj, nk;
                ni = (i + a - 1) == nm1 ? -1 : (i + a - 1);
                nj = (j + b - 1) == nm1 ? -1 : (j + b - 1);
                nk = (k + c - 1) == nm1 ? -1 : (k + c - 1);
                if (ni < 0 || nj < 0 || nk < 0) {
                  neigh[d] = -1;
                } else {
                  neigh[d] = iorder[ni + nm1 * (nj + nm1 * nk)];
                }
              }
            }
          }
          ierr = MatSetValues(K, 1, &self, 27, neigh, v, INSERT_VALUES);CHKERRQ(ierr);
        }
      }
    }
    ierr = ISRestoreIndices(invperm, &iorder);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = VecCreateSeq(PETSC_COMM_SELF,N,&B);CHKERRQ(ierr);
  ierr = VecCreateSeq(PETSC_COMM_SELF,N,&X);CHKERRQ(ierr);
  {
    PetscInt     i;
    PetscScalar *b, *x;

    ierr = VecGetArray(B,&b);CHKERRQ(ierr);
    ierr = VecGetArray(X,&x);CHKERRQ(ierr);
    for (i = 0; i < N; i++) {
      x[i] = u[order[i]];
      b[i] = f[order[i]];
    }
    ierr = VecRestoreArray(X,&x);CHKERRQ(ierr);
    ierr = VecRestoreArray(B,&b);CHKERRQ(ierr);
  }
  ierr = MatSOR(K, B, 1., SOR_FORWARD_SWEEP, 0., nits, 1, X);CHKERRQ(ierr);
  {
    PetscInt           i;
    const PetscScalar *x;

    ierr = VecGetArrayRead(X,&x);CHKERRQ(ierr);
    for (i = 0; i < N; i++) {
      u[order[i]] = x[i];
    }
    ierr = VecRestoreArrayRead(X,&x);CHKERRQ(ierr);
  }
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&B);CHKERRQ(ierr);
  ierr = MatDestroy(&K);CHKERRQ(ierr);
  ierr = ISDestroy(&invperm);CHKERRQ(ierr);
  ierr = ISDestroy(&perm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  PetscInt       test, numTests = 10;
  PetscInt       scale = 20;
  PetscRandom    rand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  cudaError_t    cerr;
  int            numDevices;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Gauss Seidel Iteration Test Options", "test_gs.c");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_tests", "Number of tests to run", "test_gs.c", numTests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-scale", "Scale (log2) of the array in the test", "test_gs.c", scale, &scale, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = PetscRandomCreate(comm, &rand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(rand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(rand);CHKERRQ(ierr);

  cerr = cudaGetDeviceCount(&numDevices); CUDA_CHK(cerr);

  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of Gauss Seidel iteration with %d devices\n", numTests, numDevices);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscScalar **f, **u, **ucheck;
    PetscReal   diff, nreal;
    PetscInt    n, nits = 1, i;
    GS          gs = NULL;
    int         d;
    struct IndexBlock *ib;
    size_t      localSize;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscRandomSetInterval(rand, PetscPowReal(2., scale / 3. - 1), PetscPowReal(2., scale / 3.));CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(rand, &nreal);CHKERRQ(ierr);

    ierr = GSCreate(comm, &gs); CHKERRQ(ierr);

    n = (PetscInt) nreal;
    ierr = MPI_Bcast(&n, 1, MPIU_INT, 0, comm);CHKERRQ(ierr);

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: [%D x %D x %D], %D iterations\n", n, n, n, nits);CHKERRQ(ierr);

    ierr = PetscMalloc4(numDevices,&f,numDevices,&u,numDevices,&ucheck,numDevices,&ib);CHKERRQ(ierr);
    ierr = GSGetFieldArraysDevice(gs, n, numDevices, ib, f, u); CHKERRQ(ierr);

    ierr = PetscRandomSetInterval(rand, -1., 1.);CHKERRQ(ierr);

    for (d = 0; d < numDevices; d++) {
      double *f_host;
      localSize = (ib[d].xEnd - ib[d].xStart) * (ib[d].yEnd - ib[d].yStart) * (ib[d].zEnd - ib[d].zStart);
      ierr = PetscMalloc1(localSize, &ucheck[d]); CHKERRQ(ierr);
      ierr = PetscMalloc1(localSize, &f_host);CHKERRQ(ierr);
      for (i = 0; i < localSize ; i++) {
        ierr = PetscRandomGetValue(rand, &f_host[i]);CHKERRQ(ierr);
        ucheck[d][i] = 0.;
      }
      cerr = cudaMemcpy(f[d], f_host, localSize * sizeof(double), cudaMemcpyHostToDevice); CUDA_CHK(cerr);
      cerr = cudaMemcpy(u[d], ucheck[d], localSize * sizeof(double), cudaMemcpyHostToDevice); CUDA_CHK(cerr);
      ierr = PetscFree(f_host);CHKERRQ(ierr);
    }

    ierr = GSIterateDevice(gs, n, nits, numDevices, ib, (const double **) f, u);CHKERRQ(ierr);

#if 0
    ierr = PetscGaussSeidelCheck(n, nits, order, f, ucheck);CHKERRQ(ierr);

    diff = 0.;
    for (i = 0; i < nm1 * nm1 * nm1; i++) {
      PetscScalar res = u[i] - ucheck[i];

      if (PetscAbsScalar(res) > PETSC_SMALL) SETERRQ3(comm, PETSC_ERR_LIB, "Test %D failed residual test at threshold %g with value %g\n", test, (double) PETSC_SMALL, (double) res);

      diff += PetscRealPart(res * PetscConj(res));
    }
    diff = PetscSqrtReal(diff);

    if (diff > PETSC_SMALL) SETERRQ3(comm, PETSC_ERR_LIB, "Test %D failed residual test at threshold %g with value %g\n", test, (double) PETSC_SMALL, (double) diff);
#endif

    ierr = GSRestoreFieldArraysDevice(gs, n, numDevices, ib, f, u); CHKERRQ(ierr);

    ierr = GSDestroy(&gs);CHKERRQ(ierr);
    for (d = 0; d < numDevices; d++) {
      ierr = PetscFree(ucheck[d]);CHKERRQ(ierr);
    }
    ierr = PetscFree4(f,u,ucheck,ib);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscViewerASCIIPrintf(viewer, "Passed.\n");CHKERRQ(ierr);

    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  ierr = PetscRandomDestroy(&rand);CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
