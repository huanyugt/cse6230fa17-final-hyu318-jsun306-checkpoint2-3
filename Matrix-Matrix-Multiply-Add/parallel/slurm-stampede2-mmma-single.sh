#!/bin/sh
#SBATCH  -J mmma-single                  # Job name
#SBATCH  -p development                  # Queue (development or normal)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --tasks-per-node 64              # Number of tasks per node
#SBATCH  -t 00:10:00                     # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035                 # Allocation
#SBATCH  -o mmma-single-%j.out           # Standard output and error log

module use /home1/01236/tisaac/opt/modulefiles
module load petsc/cse6230-single

make test_mmma

git rev-parse HEAD

git diff-files

pwd; hostname; date

ibrun tacc_affinity ./test_mmma

date
