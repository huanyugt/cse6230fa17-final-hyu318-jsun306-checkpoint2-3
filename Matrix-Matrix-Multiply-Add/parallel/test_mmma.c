const char help[] = "Test driver for the correctness of matrix-matrix multiply-add implementation";

#include <petscviewer.h>
#include <petscblaslapack.h>
#include "mmma.h"

int main(int argc, char **argv)
{
  PetscInt       test, numTests = 10;
  PetscInt       scale = 20;
  PetscRandom    globalrand;
  PetscRandom    selfrand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  MMMA           mmma = NULL;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Matrix-Matrix Multiply-Add Test Options", "test_mmma.c");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_tests", "Number of tests to run", "test_mmma.c", numTests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-scale", "Scale (log2) of the matrices in the test", "test_mmma.c", scale, &scale, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = PetscRandomCreate(comm, &globalrand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(globalrand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(globalrand);CHKERRQ(ierr);
  ierr = PetscRandomCreate(PETSC_COMM_SELF, &selfrand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(selfrand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(selfrand);CHKERRQ(ierr);

  ierr = MMMACreate(comm, &mmma); CHKERRQ(ierr);

#if defined(PETSC_USE_REAL_DOUBLE)
  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of MMMAApplyDouble()\n", numTests);CHKERRQ(ierr);
#elif defined(PETSC_USE_REAL_SINGLE)
  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of MMMAApplySingle()\n", numTests);CHKERRQ(ierr);
#else
    SETERRQ(comm,PETSC_ERR_LIB,"Precision not supported by test");
#endif
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscScalar       *Aa, *Ba, *Ca, *Da = NULL, *Ea = NULL;
    PetscReal          logm, logn, logr, alpha;
    struct MatrixBlock cBlock, aBlock, bBlock;
    PetscInt           m, n, r, i, nClocal, nAlocal, nBlocal;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscRandomSetInterval(selfrand, 0, (PetscReal) scale);CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(selfrand, &logm);CHKERRQ(ierr);
    logn = scale - logm;

    ierr = PetscRandomSetInterval(selfrand, 0, PetscMin(logn, logm));CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(selfrand, &logr);CHKERRQ(ierr);

    m = (PetscInt) PetscPowReal(2., logm);
    n = (PetscInt) PetscPowReal(2., logn);
    r = (PetscInt) PetscPowReal(2., logr);

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: C = [%D x %D], A = [%D x %D], B = [%D x %D]\n", m, n, m, r, r, n);CHKERRQ(ierr);

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = MMMAGetMatrixArraysDouble(mmma, m, n, r, &cBlock, &aBlock, &bBlock, &Ca, &Aa, &Ba); CHKERRQ(ierr);
#else
    ierr = MMMAGetMatrixArraysSingle(mmma, m, n, r, &cBlock, &aBlock, &bBlock, &Ca, &Aa, &Ba); CHKERRQ(ierr);
#endif
    nClocal = (cBlock.rowEnd - cBlock.rowStart) * (cBlock.colEnd - cBlock.colStart);
    nAlocal = (aBlock.rowEnd - aBlock.rowStart) * (aBlock.colEnd - aBlock.colStart);
    nBlocal = (bBlock.rowEnd - bBlock.rowStart) * (bBlock.colEnd - bBlock.colStart);

    ierr = PetscRandomSetInterval(globalrand, -1., 1.);CHKERRQ(ierr);

    ierr = PetscRandomGetValue(selfrand, &alpha);CHKERRQ(ierr);

    for (i = 0; i < nClocal; i++) {
      ierr = PetscRandomGetValue(globalrand, &Ca[i]);CHKERRQ(ierr);
    }
    for (i = 0; i < nAlocal; i++) {
      ierr = PetscRandomGetValue(globalrand, &Aa[i]);CHKERRQ(ierr);
    }
    for (i = 0; i < nBlocal; i++) {
      ierr = PetscRandomGetValue(globalrand, &Ba[i]);CHKERRQ(ierr);
    }

    {
      int    rank, size;
      struct MatrixBlock *cBlockGlobal = NULL;
      PetscScalar *recvbuf;
      PetscMPIInt *recvcounts = NULL;
      PetscMPIInt *displs = NULL;

      ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRQ(ierr);
      ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank); CHKERRQ(ierr);

      if (!rank) {
        ierr = PetscMalloc1(size,&cBlockGlobal); CHKERRQ(ierr);
        ierr = PetscMalloc2(size,&recvcounts,size,&displs); CHKERRQ(ierr);
        ierr = PetscMalloc1(m*n, &recvbuf); CHKERRQ(ierr);
      }
      ierr = MPI_Gather(&cBlock, sizeof(struct MatrixBlock), MPI_BYTE, cBlockGlobal, sizeof(struct MatrixBlock), MPI_BYTE, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
      if (!rank) {
        displs[0] = 0;
        for (int p = 0; p < size; p++) {
          recvcounts[p] = (cBlockGlobal[p].colEnd - cBlockGlobal[p].colStart) * (cBlockGlobal[p].rowEnd - cBlockGlobal[p].rowStart);
          if (p < size - 1) {
            displs[p + 1] = displs[p] + recvcounts[p];
          }
        }
      }
      ierr = MPI_Gatherv(Ca,(cBlock.colEnd - cBlock.colStart) * (cBlock.rowEnd - cBlock.rowStart), MPIU_SCALAR, recvbuf, recvcounts, displs, MPIU_SCALAR, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
      if (!rank) {
        PetscInt  offset = 0;

        ierr = PetscMalloc2(m*n,&Da,m*n,&Ea); CHKERRQ(ierr);
        for (PetscInt p = 0; p < size; p++) {
          PetscInt rowStart = cBlockGlobal[p].rowStart;
          PetscInt rowEnd = cBlockGlobal[p].rowEnd;
          PetscInt colStart = cBlockGlobal[p].colStart;
          PetscInt colEnd = cBlockGlobal[p].colEnd;

          for (int i = rowStart; i < rowEnd; i++) {
            for (int j = colStart; j < colEnd; j++) {
              Da[i * n + j] = recvbuf[offset++];
              Ea[i * n + j] = PetscAbsReal(Da[i * n + j]);
            }
          }
        }

        ierr = PetscFree(recvbuf); CHKERRQ(ierr);
        ierr = PetscFree2(recvcounts,displs); CHKERRQ(ierr);
        ierr = PetscFree(cBlockGlobal); CHKERRQ(ierr);
      }
    }

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = MMMAApplyDouble(mmma, m, n, r, alpha, &cBlock, &aBlock, &bBlock, Ca, Aa, Ba); CHKERRQ(ierr);
#else
    ierr = MMMAApplySingle(mmma, m, n, r, alpha, &cBlock, &aBlock, &bBlock, Ca, Aa, Ba); CHKERRQ(ierr);
#endif

    {
      int    rank, size;
      struct MatrixBlock *aBlockGlobal = NULL;
      struct MatrixBlock *bBlockGlobal = NULL;
      struct MatrixBlock *cBlockGlobal = NULL;
      PetscScalar *As = NULL, *Bs = NULL, *Cs = NULL;
      PetscScalar *recvbuf;
      PetscMPIInt *recvcounts = NULL;
      PetscMPIInt *displs = NULL;

      ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRQ(ierr);
      ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank); CHKERRQ(ierr);

      if (!rank) {
        ierr = PetscMalloc3(size,&aBlockGlobal,size,&bBlockGlobal,size,&cBlockGlobal); CHKERRQ(ierr);
        ierr = PetscMalloc3(m*n,&Cs,m*r,&As,r*n,&Bs); CHKERRQ(ierr);
        ierr = PetscMalloc2(size,&recvcounts,size,&displs); CHKERRQ(ierr);
        ierr = PetscMalloc1(PetscMax(m,r) * PetscMax(r,n), &recvbuf); CHKERRQ(ierr);
      }
      ierr = MPI_Gather(&aBlock, sizeof(struct MatrixBlock), MPI_BYTE, aBlockGlobal, sizeof(struct MatrixBlock), MPI_BYTE, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
      ierr = MPI_Gather(&bBlock, sizeof(struct MatrixBlock), MPI_BYTE, bBlockGlobal, sizeof(struct MatrixBlock), MPI_BYTE, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
      ierr = MPI_Gather(&cBlock, sizeof(struct MatrixBlock), MPI_BYTE, cBlockGlobal, sizeof(struct MatrixBlock), MPI_BYTE, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
      if (!rank) {
        displs[0] = 0;
        for (int p = 0; p < size; p++) {
          recvcounts[p] = (aBlockGlobal[p].colEnd - aBlockGlobal[p].colStart) * (aBlockGlobal[p].rowEnd - aBlockGlobal[p].rowStart);
          if (p < size - 1) {
            displs[p + 1] = displs[p] + recvcounts[p];
          }
        }
      }
      ierr = MPI_Gatherv(Aa,(aBlock.colEnd - aBlock.colStart) * (aBlock.rowEnd - aBlock.rowStart), MPIU_SCALAR, recvbuf, recvcounts, displs, MPIU_SCALAR, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
      if (!rank) {
        PetscInt offset = 0;
        for (PetscInt p = 0; p < size; p++) {
          PetscInt rowStart = aBlockGlobal[p].rowStart;
          PetscInt rowEnd = aBlockGlobal[p].rowEnd;
          PetscInt colStart = aBlockGlobal[p].colStart;
          PetscInt colEnd = aBlockGlobal[p].colEnd;

          for (int i = rowStart; i < rowEnd; i++) {
            for (int j = colStart; j < colEnd; j++) {
              As[i * r + j] = recvbuf[offset++];
            }
          }
        }
      }
      if (!rank) {
        displs[0] = 0;
        for (int p = 0; p < size; p++) {
          recvcounts[p] = (bBlockGlobal[p].colEnd - bBlockGlobal[p].colStart) * (bBlockGlobal[p].rowEnd - bBlockGlobal[p].rowStart);
          if (p < size - 1) {
            displs[p + 1] = displs[p] + recvcounts[p];
          }
        }
      }
      ierr = MPI_Gatherv(Ba,(bBlock.colEnd - bBlock.colStart) * (bBlock.rowEnd - bBlock.rowStart), MPIU_SCALAR, recvbuf, recvcounts, displs, MPIU_SCALAR, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
      if (!rank) {
        PetscInt offset = 0;
        for (PetscInt p = 0; p < size; p++) {
          PetscInt rowStart = bBlockGlobal[p].rowStart;
          PetscInt rowEnd = bBlockGlobal[p].rowEnd;
          PetscInt colStart = bBlockGlobal[p].colStart;
          PetscInt colEnd = bBlockGlobal[p].colEnd;

          for (int i = rowStart; i < rowEnd; i++) {
            for (int j = colStart; j < colEnd; j++) {
              Bs[i * n + j] = recvbuf[offset++];
            }
          }
        }
      }
      if (!rank) {
        displs[0] = 0;
        for (int p = 0; p < size; p++) {
          recvcounts[p] = (cBlockGlobal[p].colEnd - cBlockGlobal[p].colStart) * (cBlockGlobal[p].rowEnd - cBlockGlobal[p].rowStart);
          if (p < size - 1) {
            displs[p + 1] = displs[p] + recvcounts[p];
          }
        }
      }
      ierr = MPI_Gatherv(Ca,(cBlock.colEnd - cBlock.colStart) * (cBlock.rowEnd - cBlock.rowStart), MPIU_SCALAR, recvbuf, recvcounts, displs, MPIU_SCALAR, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
      if (!rank) {
        PetscInt  offset = 0;
        PetscReal margin, maxThresh;

        for (PetscInt p = 0; p < size; p++) {
          PetscInt rowStart = cBlockGlobal[p].rowStart;
          PetscInt rowEnd = cBlockGlobal[p].rowEnd;
          PetscInt colStart = cBlockGlobal[p].colStart;
          PetscInt colEnd = cBlockGlobal[p].colEnd;

          for (int i = rowStart; i < rowEnd; i++) {
            for (int j = colStart; j < colEnd; j++) {
              Cs[i * n + j] = recvbuf[offset++];
            }
          }
        }

        {
          PetscBLASInt mb = m, nb = n, rb = r;
          PetscReal one = 1.;

          PetscStackCallBLAS("BLASgemm", BLASgemm_("N", "N", &nb, &mb, &rb, &one, Bs, &nb, As, &rb, &alpha, Da, &nb));
        }

        for (int i = 0; i < m * r; i++) {
          As[i] = PetscAbsReal(As[i]);
        }
        for (int i = 0; i < r * n; i++) {
          Bs[i] = PetscAbsReal(Bs[i]);
        }

        alpha = PetscAbsReal(alpha);

        {
          PetscBLASInt mb = m, nb = n, rb = r;
          PetscReal one = 1.;

          PetscStackCallBLAS("BLASgemm", BLASgemm_("N", "N", &nb, &mb, &rb, &one, Bs, &nb, As, &rb, &alpha, Ea, &nb));
        }

        margin = PetscPowReal(((PetscReal) 1. + (PetscReal) PETSC_MACHINE_EPSILON),2. * (2. * r + 1)) - 1.;
        maxThresh = 0.;
        for (i = 0; i < m *  n; i++) {
          PetscReal res = Da[i] - Cs[i];
          PetscReal thresh = margin * Ea[i];

          maxThresh = PetscMax(thresh,maxThresh);
          if (PetscAbsReal(res) > thresh) {
            SETERRQ5(comm, PETSC_ERR_LIB, "Test %D failed error test at (%D,%D) threshold %g with value %g\n", test, i / n, i % n, (double) thresh, (double) res);
          }
        }
        ierr = PetscFree2(Da,Ea); CHKERRQ(ierr);
        ierr = PetscFree(recvbuf); CHKERRQ(ierr);
        ierr = PetscFree2(recvcounts,displs); CHKERRQ(ierr);
        ierr = PetscFree3(Cs,As,Bs); CHKERRQ(ierr);
        ierr = PetscFree3(aBlockGlobal,bBlockGlobal,cBlockGlobal); CHKERRQ(ierr);
      }
    }

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = MMMARestoreMatrixArraysDouble(mmma, m, n, r, &cBlock, &aBlock, &bBlock, &Ca, &Aa, &Ba); CHKERRQ(ierr);
#else
    ierr = MMMARestoreMatrixArraysSingle(mmma, m, n, r, &cBlock, &aBlock, &bBlock, &Ca, &Aa, &Ba); CHKERRQ(ierr);
#endif

    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&selfrand);CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&globalrand);CHKERRQ(ierr);

  ierr = MMMADestroy(&mmma); CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
