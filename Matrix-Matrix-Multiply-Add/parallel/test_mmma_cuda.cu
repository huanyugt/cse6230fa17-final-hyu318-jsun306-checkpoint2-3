const char help[] = "Test driver for the correctness of matrix-matrix multiply-add implementation";

#include <petscviewer.h>
#include <petscblaslapack.h>
#include <curand.h>
#include <cublas_v2.h>
#include "mmma.h"

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error %s", cudaGetErrorString(_cerr));} while(0)
#define CURAND_CHK(crs) do {curandStatus_t _crs = (crs); if ((_crs) != CURAND_STATUS_SUCCESS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Curand error");} while(0)
#define CUBLAS_CHK(cbs) do {cublasStatus_t _cbs = (cbs); if ((_cbs) != CUBLAS_STATUS_SUCCESS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cublas error");} while(0)

void __global__ scaleRandom(PetscInt count, PetscScalar *array)
{
  PetscInt tid      = blockDim.x * blockIdx.x + threadIdx.x;
  PetscInt gridSize = gridDim.x * blockDim.x;

  for (PetscInt i = tid; i < count; i += gridSize) {
    array[i] = 2. * array[i] - 1.;
  }
}

void __global__ arrayAbs(PetscInt count, PetscScalar *array)
{
  PetscInt tid      = blockDim.x * blockIdx.x + threadIdx.x;
  PetscInt gridSize = gridDim.x * blockDim.x;

  for (PetscInt i = tid; i < count; i += gridSize) {
    array[i] = fabs(array[i]);
  }
}

void __global__ arrayScale(PetscInt count, PetscScalar scale, PetscScalar *array)
{
  PetscInt tid      = blockDim.x * blockIdx.x + threadIdx.x;
  PetscInt gridSize = gridDim.x * blockDim.x;

  for (PetscInt i = tid; i < count; i += gridSize) {
    array[i] *= scale;
  }
}

static PetscErrorCode setRandomMatrixBlock(PetscInt localSize, curandGenerator_t gen, dim3 block, PetscScalar *d_array)
{
  dim3 grid;
  curandStatus_t crs;

  PetscFunctionBegin;
#if defined(PETSC_USE_REAL_DOUBLE)
  crs = curandGenerateUniformDouble(gen, d_array, localSize); CURAND_CHK(crs);
#else
  crs = curandGenerateUniform(gen, d_array, localSize); CURAND_CHK(crs);
#endif
  grid = dim3((localSize + block.x - 1) / block.x);
  scaleRandom <<<grid, block>>>(localSize, d_array);

#if defined(DEBUG)
  {
    PetscScalar    *h_array;
    PetscErrorCode  ierr;
    cudaError_t     cerr;

    cerr = cudaDeviceSynchronize(); CUDA_CHK(cerr);
    cerr = cudaMallocHost(&h_array, localSize * sizeof(PetscScalar)); CUDA_CHK(cerr);
    cerr = cudaMemcpy(h_array, d_array, localSize * sizeof(PetscScalar), cudaMemcpyDeviceToHost); CUDA_CHK(cerr);
    for (int i = 0; i < localSize;) {
      for (int j = 0; i < localSize && j < 10; j++, i++) {
        ierr = PetscPrintf(PETSC_COMM_SELF, "%g ", h_array[i]); CHKERRQ(ierr);
      }
      ierr = PetscPrintf(PETSC_COMM_SELF, "\n"); CHKERRQ(ierr);
    }
    cerr = cudaFreeHost(h_array); CUDA_CHK(cerr);
  }
#endif
  PetscFunctionReturn(0);
}

static PetscErrorCode MMMAApply(PetscInt M, PetscInt N, PetscInt R, PetscInt numDevices,
                                PetscScalar alpha,
                                const struct MatrixBlock *cBlock,
                                const struct MatrixBlock *aBlock,
                                const struct MatrixBlock *bBlock,
                                PetscScalar *C[], PetscScalar *A[], PetscScalar *B[])
{
  int                   block, grid;
  cudaError_t           cerr;
  PetscErrorCode        ierr;
  struct cudaDeviceProp prop;

  PetscFunctionBegin;
  cerr = cudaGetDeviceProperties(&prop, 0); CUDA_CHK(cerr);
  block = prop.maxThreadsPerBlock;
  for (int i = 0; i < numDevices; i++) {
    PetscScalar    *c = C[i];
    PetscInt       rowC = (cBlock[i].rowEnd - cBlock[i].rowStart);
    PetscInt       colC = (cBlock[i].colEnd - cBlock[i].colStart);
    PetscInt       nC = rowC * colC;
    cublasHandle_t handle;
    cublasStatus_t cbs;

    cerr = cudaSetDevice(i); CUDA_CHK(cerr);
    grid = (nC + block - 1) / block;
    /* scale C separately, because we may have to add multiple gemm contributions together */
    arrayScale<<<grid, block>>>(nC, alpha, C[i]);
    cbs = cublasCreate(&handle); CUBLAS_CHK(cbs);
    for (int j = 0; j < numDevices; j++) {
      PetscInt mStart, mEnd;
      PetscInt rowA = (aBlock[j].rowEnd - aBlock[j].rowStart);
      PetscInt colA = (aBlock[j].colEnd - aBlock[j].colStart);
      PetscInt nA = rowA * colA;

      mStart = PetscMax(cBlock[i].rowStart, aBlock[j].rowStart);
      mEnd   = PetscMin(cBlock[i].rowEnd,   aBlock[j].rowEnd);
      if (mStart >= mEnd) continue;
      for (int k = 0; k < numDevices; k++) {
        PetscInt nStart, nEnd, rStart, rEnd;
        PetscInt rowB = (bBlock[k].rowEnd - bBlock[k].rowStart);
        PetscInt colB = (bBlock[k].colEnd - bBlock[k].colStart);
        PetscInt nB = rowB * colB;
        PetscScalar *a, *b, one = 1., one2 = 1.;

        nStart = PetscMax(cBlock[i].colStart, bBlock[k].colStart);
        nEnd   = PetscMin(cBlock[i].colEnd,   bBlock[k].colEnd);
        if (nStart >= nEnd) continue;
        rStart = PetscMax(aBlock[j].colStart, bBlock[k].rowStart);
        rEnd   = PetscMin(aBlock[j].colEnd,   bBlock[k].rowEnd);
        if (rStart >= rEnd) continue;
        ierr = PetscPrintf(PETSC_COMM_SELF, "GEMM block: [%D:%D,%D:%D] (%d) += [%D:%D,%D:%D] (%d) * [%D:%D,%D:%D] (%d)\n",
                           mStart, mEnd, nStart, nEnd, i, mStart, mEnd, rStart, rEnd, j, rStart, rEnd, nStart, nEnd, k); CHKERRQ(ierr);
        if (i == j) {
          a = A[j];
        } else {
          cerr = cudaMalloc(&a,nA * sizeof(PetscScalar)); CUDA_CHK(cerr);
          cerr = cudaMemcpyPeer(a, i, A[j], j, nA * sizeof(PetscScalar)); CUDA_CHK(cerr);
        }
        if (i == k) {
          b = B[k];
        } else {
          cerr = cudaMalloc(&b,nB * sizeof(PetscScalar)); CUDA_CHK(cerr);
          cerr = cudaMemcpyPeer(b, i, B[k], k, nB * sizeof(PetscScalar)); CUDA_CHK(cerr);
        }

        /* now we have the necessary blocks on device i: call cuBLAS */
#if defined(PETSC_USE_REAL_DOUBLE)
        cbs = cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N,
                          (nEnd - nStart), (mEnd - mStart), (rEnd - rStart),
                          &one,
                          &b[colB * (rStart - bBlock[k].rowStart) + nStart - bBlock[k].colStart],
                          colB,
                          &a[colA * (mStart - aBlock[j].rowStart) + rStart - aBlock[j].colStart],
                          colA,
                          &one2,
                          &c[colC * (mStart - cBlock[i].rowStart) + nStart - cBlock[i].colStart],
                          colC); CUBLAS_CHK(cbs);
#else
        cbs = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N,
                          (nEnd - nStart), (mEnd - mStart), (rEnd - rStart),
                          &one,
                          &b[colB * (rStart - bBlock[k].rowStart) + nStart - bBlock[k].colStart],
                          colB,
                          &a[colA * (mStart - aBlock[j].rowStart) + rStart - aBlock[j].colStart],
                          colA,
                          &one2,
                          &c[colC * (mStart - cBlock[i].rowStart) + nStart - cBlock[i].colStart],
                          colC); CUBLAS_CHK(cbs);
#endif
        if (i != k) {
          cerr = cudaFree(b); CUDA_CHK(cerr);
        }
        if (i != j) {
          cerr = cudaFree(a); CUDA_CHK(cerr);
        }
      }
    }
    cbs = cublasDestroy(handle); CUBLAS_CHK(cbs);
  }
  cerr = cudaSetDevice(0); CUDA_CHK(cerr);
  PetscFunctionReturn(0);
}


static PetscErrorCode TestMMMACheck(PetscInt M, PetscInt N, PetscInt R, PetscInt numDevices,
                                    PetscScalar alpha,
                                    const struct MatrixBlock *cBlock,
                                    const struct MatrixBlock *aBlock,
                                    const struct MatrixBlock *bBlock,
                                    PetscScalar *C[], PetscScalar *A[], PetscScalar *B[],
                                    PetscScalar *D[], PetscScalar *E[])
{
  struct cudaDeviceProp prop;
  cudaError_t    cerr;
  int            block, grid;
  PetscReal      margin = PetscPowReal(((PetscReal) 1. + (PetscReal) PETSC_MACHINE_EPSILON),2. * (2. * R + 1)) - 1.;
  PetscReal      maxThresh = 0.;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = MMMAApply(M, N, R, numDevices, alpha, cBlock, aBlock, bBlock, D, A, B); CHKERRQ(ierr);
  cerr = cudaGetDeviceProperties(&prop, 0); CUDA_CHK(cerr);
  block = prop.maxThreadsPerBlock;
  for (int i = 0; i < numDevices; i++) {
    PetscInt arraySize;

    cerr = cudaSetDevice(i); CUDA_CHK(cerr);

    arraySize = (aBlock[i].rowEnd - aBlock[i].rowStart) * (aBlock[i].colEnd - aBlock[i].colStart);
    grid = (arraySize + block - 1) / block;
    arrayAbs<<<grid, block>>>(arraySize, A[i]);

    arraySize = (bBlock[i].rowEnd - bBlock[i].rowStart) * (bBlock[i].colEnd - bBlock[i].colStart);
    grid = (arraySize + block - 1) / block;
    arrayAbs<<<grid, block>>>(arraySize, B[i]);
  }
  cerr = cudaSetDevice(0); CUDA_CHK(cerr);

  ierr = MMMAApply(M, N, R, numDevices, PetscAbsReal(alpha), cBlock, aBlock, bBlock, E, A, B); CHKERRQ(ierr);

  for (int i = 0; i < numDevices; i++) {
    PetscInt rowC, colC, arraySize;
    PetscScalar *h_C, *h_D, *h_E;

    cerr = cudaSetDevice(i); CUDA_CHK(cerr);

    rowC = (cBlock[i].rowEnd - cBlock[i].rowStart);
    colC = (cBlock[i].colEnd - cBlock[i].colStart);
    arraySize = rowC * colC;

    ierr = PetscMalloc3(arraySize, &h_C, arraySize, &h_D, arraySize, &h_E); CHKERRQ(ierr);

    cerr = cudaMemcpy(h_C,C[i],arraySize*sizeof(PetscScalar),cudaMemcpyDeviceToHost); CHKERRQ(ierr);
    cerr = cudaMemcpy(h_D,D[i],arraySize*sizeof(PetscScalar),cudaMemcpyDeviceToHost); CHKERRQ(ierr);
    cerr = cudaMemcpy(h_E,E[i],arraySize*sizeof(PetscScalar),cudaMemcpyDeviceToHost); CHKERRQ(ierr);

    for (int j = 0; j < arraySize; j++) {
      PetscReal res = h_D[j] - h_C[j];
      PetscReal thresh = margin * h_E[j];

      maxThresh = PetscMax(thresh,maxThresh);
      if (PetscAbsReal(res) > thresh) {
        SETERRQ7(PETSC_COMM_SELF, PETSC_ERR_LIB, "Failed error test at (%D,%D) threshold %g with value %g, (C %g, D %g, E %g)\n", (j / colC) + cBlock[i].rowStart, (j % colC) + cBlock[i].colStart, (double) thresh, (double) res, (double) h_C[j], (double) h_D[j], (double) h_E[j]);
      }
    }
    ierr = PetscFree3(h_C, h_D, h_E); CHKERRQ(ierr);
  }
  cerr = cudaSetDevice(0); CUDA_CHK(cerr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "Maximum threshold: %g\n", (double) maxThresh);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "Passed.\n");CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  PetscInt       test, numTests = 10;
  PetscInt       scale = 20;
  PetscRandom    rand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  MMMA           mmma = NULL;
  int            numDevices, size;
  cudaError_t    cerr;
  curandStatus_t crs;
  struct MatrixBlock *cBlock, *aBlock, *bBlock;
  PetscScalar    **Aa, **Ba, **Ca, **Da, **Ea;
  curandGenerator_t *gen;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  if (size != 1) SETERRQ(comm, PETSC_ERR_ARG_OUTOFRANGE, "This is a serial MPI program, run with one process\n");
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Matrix-Matrix Multiply-Add Test Options", "test_mmma.c");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_tests", "Number of tests to run", "test_mmma.c", numTests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-scale", "Scale (log2) of the matrices in the test", "test_mmma.c", scale, &scale, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = PetscRandomCreate(comm, &rand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(rand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(rand);CHKERRQ(ierr);

  ierr = MMMACreate(comm, &mmma); CHKERRQ(ierr);

  cerr = cudaGetDeviceCount(&numDevices); CUDA_CHK(cerr);
  ierr = PetscMalloc1(numDevices, &gen); CHKERRQ(ierr);
  for (int d = 0; d < numDevices; d++) {
    cerr = cudaSetDevice(d); CUDA_CHK(cerr);
    crs = curandCreateGenerator(&gen[d], CURAND_RNG_PSEUDO_DEFAULT); CURAND_CHK(crs);
    crs = curandSetPseudoRandomGeneratorSeed(gen[d],d); CURAND_CHK(crs);
  }
  cerr = cudaSetDevice(0); CUDA_CHK(cerr);
  ierr = PetscMalloc3(numDevices, &cBlock, numDevices, &aBlock, numDevices, &bBlock); CHKERRQ(ierr);
  ierr = PetscMalloc5(numDevices, &Aa, numDevices, &Ba, numDevices, &Ca, numDevices, &Da, numDevices, &Ea); CHKERRQ(ierr);
#if defined(PETSC_USE_REAL_DOUBLE)
  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of MMMAApplyDoubleDevice() with %d devices\n", numTests, numDevices);CHKERRQ(ierr);
#elif defined(PETSC_USE_REAL_SINGLE)
  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of MMMAApplyDoubleDevice() with %d devices\n", numTests, numDevices);CHKERRQ(ierr);
#else
  SETERRQ(comm,PETSC_ERR_LIB,"Precision not supported by test");
#endif
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscReal          logm, logn, logr, alpha;
    PetscInt           m, n, r;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscRandomSetInterval(rand, ((PetscReal) scale) / 3., ((PetscReal) scale) * 2. / 3.);CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(rand, &logm);CHKERRQ(ierr);
    logn = scale - logm;

    ierr = PetscRandomSetInterval(rand, PetscMin(logn, logm) / 2., PetscMin(logn, logm));CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(rand, &logr);CHKERRQ(ierr);

    m = (PetscInt) PetscPowReal(2., logm);
    n = (PetscInt) PetscPowReal(2., logn);
    r = (PetscInt) PetscPowReal(2., logr);

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: C = [%D x %D], A = [%D x %D], B = [%D x %D]\n", m, n, m, r, r, n);CHKERRQ(ierr);

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = MMMAGetMatrixArraysDoubleDevice(mmma, m, n, r, numDevices, cBlock, aBlock, bBlock, Ca, Aa, Ba); CHKERRQ(ierr);
#else
    ierr = MMMAGetMatrixArraysSingleDevice(mmma, m, n, r, numDevices, cBlock, aBlock, bBlock, Ca, Aa, Ba); CHKERRQ(ierr);
#endif

    ierr = PetscRandomSetInterval(rand, -1., 1.);CHKERRQ(ierr);
    ierr = PetscRandomGetValue(rand, &alpha);CHKERRQ(ierr);

    for (int d = 0; d < numDevices; d++) {
      PetscInt nClocal, nAlocal, nBlocal;
      dim3     block, grid;
      struct cudaDeviceProp prop;

      cerr = cudaSetDevice(d); CUDA_CHK(cerr);
      cerr = cudaGetDeviceProperties(&prop, d); CUDA_CHK(cerr);
      block = dim3(prop.maxThreadsPerBlock);
      nClocal = (cBlock[d].rowEnd - cBlock[d].rowStart) * (cBlock[d].colEnd - cBlock[d].colStart);
      nAlocal = (aBlock[d].rowEnd - aBlock[d].rowStart) * (aBlock[d].colEnd - aBlock[d].colStart);
      nBlocal = (bBlock[d].rowEnd - bBlock[d].rowStart) * (bBlock[d].colEnd - bBlock[d].colStart);
      if (nClocal){
        ierr = setRandomMatrixBlock(nClocal, gen[d], block, Ca[d]); CHKERRQ(ierr);
      }
      if (nAlocal){
        ierr = setRandomMatrixBlock(nAlocal, gen[d], block, Aa[d]); CHKERRQ(ierr);
      }
      if (nBlocal) {
        ierr = setRandomMatrixBlock(nBlocal, gen[d], block, Ba[d]); CHKERRQ(ierr);
      }
      cerr = cudaMalloc(&Da[d], nClocal * sizeof(PetscScalar)); CHKERRQ(ierr);
      cerr = cudaMalloc(&Ea[d], nClocal * sizeof(PetscScalar)); CHKERRQ(ierr);
      cerr = cudaMemcpy(Da[d], Ca[d], nClocal * sizeof(PetscScalar), cudaMemcpyDeviceToDevice); CHKERRQ(ierr);
      cerr = cudaMemcpy(Ea[d], Ca[d], nClocal * sizeof(PetscScalar), cudaMemcpyDeviceToDevice); CHKERRQ(ierr);
      grid = dim3((nClocal + block.x - 1)/block.x);
      arrayAbs<<<grid, block>>>(nClocal, Ea[d]);
    }
    cerr = cudaSetDevice(0); CUDA_CHK(cerr);

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = MMMAApplyDoubleDevice(mmma, m, n, r, alpha, numDevices, cBlock, aBlock, bBlock, Ca, (const PetscScalar **) Aa, (const PetscScalar **) Ba); CHKERRQ(ierr);
#else
    ierr = MMMAApplySingleDevice(mmma, m, n, r, alpha, numDevices, cBlock, aBlock, bBlock, Ca, (const PetscScalar **) Aa, (const PetscScalar **) Ba); CHKERRQ(ierr);
#endif

    ierr = TestMMMACheck(m, n, r, numDevices, alpha, cBlock, aBlock, bBlock, Ca, Aa, Ba, Da, Ea); CHKERRQ(ierr);

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = MMMARestoreMatrixArraysDoubleDevice(mmma, m, n, r, numDevices, cBlock, aBlock, bBlock, Ca, Aa, Ba); CHKERRQ(ierr);
#else
    ierr = MMMARestoreMatrixArraysSingleDevice(mmma, m, n, r, numDevices, cBlock, aBlock, bBlock, Ca, Aa, Ba); CHKERRQ(ierr);
#endif

    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = PetscFree5(Aa, Ba, Ca, Da, Ea); CHKERRQ(ierr);
  ierr = PetscFree3(cBlock, aBlock, bBlock); CHKERRQ(ierr);
  for (int d = 0; d < numDevices; d++) {
    cerr = cudaSetDevice(d); CUDA_CHK(cerr);
    crs = curandDestroyGenerator(gen[d]); CURAND_CHK(crs);
  }
  cerr = cudaSetDevice(0); CUDA_CHK(cerr);
  ierr = PetscFree(gen); CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&rand);CHKERRQ(ierr);

  ierr = MMMADestroy(&mmma); CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
/* vi: set expandtab sw=2 ts=2 cindent: */
