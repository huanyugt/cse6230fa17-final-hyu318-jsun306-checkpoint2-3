
#include <petscsys.h>
#include "lbm.h"

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error");} while(0)

int LBMGetFieldArrayDevice(LBM lbm, size_t N, int numDevices,
                           struct IndexBlock iBlock[],
                           double (*fLocal_p[])[27])
{
  cudaError_t       cerr;

  PetscFunctionBeginUser;

  iBlock[0].xStart = 0;
  iBlock[0].xEnd   = N;
  iBlock[0].yStart = 0;
  iBlock[0].yEnd   = N;
  iBlock[0].zStart = 0;
  iBlock[0].zEnd   = N;
  cerr = cudaMalloc(&fLocal_p[0], N * N * N * sizeof(*fLocal_p[0])); CUDA_CHK(cerr);

  for (int i = 1; i < numDevices; i++) {
    iBlock[i].xStart = N;
    iBlock[i].xEnd   = N;
    iBlock[i].yStart = N;
    iBlock[i].yEnd   = N;
    iBlock[i].zStart = N;
    iBlock[i].zEnd   = N;
    fLocal_p[i] = NULL;
  }

  PetscFunctionReturn(0);
}

int LBMRestoreFieldArrayDevice(LBM lbm, size_t N, int numDevices,
                               struct IndexBlock iBlock[],
                               double (*fLocal_p[])[27])
{
  cudaError_t    cerr;

  PetscFunctionBeginUser;
  cerr = cudaFree(fLocal_p[0]); CUDA_CHK(cerr);
  PetscFunctionReturn(0);
}

int LBMIterateDevice(LBM lbm, size_t N, int num_iter, double tau,
                     int numDevices,
                     const struct IndexBlock iBlock[],
                     double (*fLocal[])[27])
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}
