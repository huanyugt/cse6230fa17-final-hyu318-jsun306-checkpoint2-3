#if !defined(DFT_H)
#define      DFT_H

#include <stddef.h>
#include <complex.h>

/** 3D Discrete Fourier Transform

  \param[in]  n       Size of the three dimensional array in each direction.
                      You may assume this is a power of 2.
  \param[in]  x       [n x n x n] array.
  \param[out] y       [n x n x n] array, the discrete Fourier transform of x.
 */
int discrete_fourier_transform (size_t n, const double _Complex *x, double _Complex *y);

#endif
